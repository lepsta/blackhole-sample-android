package com.blackholeio.blackholeapp;

import android.app.Application;

import com.blackholeio.lite.Blackhole;

public class BlackholeApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // Initialize Blackhole
        Blackhole.init(this);
    }

}
